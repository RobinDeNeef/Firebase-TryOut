const functions = require('firebase-functions');
const express = require('express')
const bodyParser = require('body-parser')
const sanitizer = require('express-sanitizer')
const admin = require('firebase-admin')
var session = require('express-session');

var serviceAccount= require('./serviceAccountKey.json')

var firebaseAdmin = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://myawesomeproject-c4805.firebaseio.com'
})

var app = express()

app.set('view engine', 'ejs')

app.use(bodyParser.urlencoded({extended: true}))
app.use(session({secret: "Shh, its a secret!"}));
app.use(sanitizer())

// Create Authentication Middleware
function isAuthenticated(req, res, next){
    // check if user is logged in
    // if they are, attach the user to the request object and call next
    // if not, send them to the login page
}

app.get('/todo', isAuthenticated, function(req, res){
    res.render('todo.ejs')
})

app.get('/', function(req, res){
    res.render("landing")
})

app.get('/login', function(req, res){
    res.render("user/login")
})

app.post('/login', function(req, res){
    var idToken = req.body.idtoken
    admin.auth().verifyIdToken(idToken)
    .then(function(decodedToken) {
        var uid = decodedToken.uid;
        console.log(uid)
    }).catch(function(error) {
        // Handle error
    });
    res.redirect('/')
})

app.get('/register', function(req, res){
    res.render("user/register")
})

exports.app = functions.https.onRequest(app);
